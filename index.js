const { ShardingManager } = require('discord.js');
const { token } = require('./config.json');
const manager = new ShardingManager('./bot.js', { token: token, shardArgs: ['--ansi', '--color', '--trace-warnings'] });

manager.spawn();
manager.on('launch', shard => console.log(`Launched shard ${shard.id}`));
manager.on('message', (shard, message) => {
    console.log(`Shard[${shard.id}] : ${message._eval} : ${message._result}`);
});