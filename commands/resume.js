module.exports = {
    name: 'resume',
    description: 'Resumes the currently playing song.',
    args: false,
    guildOnly: true,
    testing: false,
    cooldown: 5,
    execute(message, a, queue) {
        const serverQueue = queue.get(message.guild.id);
        if (serverQueue && !serverQueue.playing) {
            serverQueue.playing = true;
            serverQueue.connection.dispatcher.resume();
            return message.channel.send('▶ Resumed the playing music!');
        }
        return message.channel.send('There is nothing playing!');
    }
};