module.exports = {
    name: 'queue',
    description: 'Check the queue of the list of songs that are being played.',
    guildOnly: true,
    testing: false,
    aliases: ['q', 'playingsoon'],
    execute(message, a, queue) {
        const serverQueue = queue.get(message.guild.id);
        if (!serverQueue) return message.channel.send('The queue is empty! I think it is about time to add songs, don\'t you think?');
        return message.channel.send(`
    __**Song queue:**__\n${serverQueue.songs.map(song => `**-** ${song.title}: \`Requested by: ${song.requester}\``).join('\n')}
    **Now playing:** ${serverQueue.songs[0].title}
        `);
        // const queue = module.exports.servers;
        // if (queue.queue === undefined) {
        //     return message.channel.send('It seems like the servers is empty... Try adding some songs! :D');
        // }
        // const tosend = [];
        // queue[message.guild.id].queue.forEach((song, i) => {
        //     tosend.push(`${i + 1}. ${song.title} - Requested by: ${song.requester}`);
        // });
        // message.channel.send(`__**${message.guild.name}'s Music Queue:**__Currently **${tosend.length}** songs queued ${(tosend.length > 15 ? '*[Only next 15 shown]*' : '')}\n\`\`\`${tosend.slice(0, 15).join('\n')}\`\`\`'`);
    }
};
