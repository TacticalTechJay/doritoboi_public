module.exports = {
	name: 'prune',
	description: 'Remove those messages all in one scoop!',
	guildOnly: true,
	testing: false,
	args: true,
	aliases: ['clear', 'purge'],
	execute(message, args) {
		if (!message.guild.me.hasPermission('MANAGE_MESSAGES', false, true, false)) {
			return message.channel.send('I do not have permissions to prune commands!');
		}
		if(!message.member.hasPermission('MANAGE_MESSAGES', false, true, true)) {
			return message.channel.send('You do not have permission to scoop up these commands.');
		}
		const deleteCount = parseInt(args[0], 10);

		if(!deleteCount || deleteCount < 2 || deleteCount > 100) {
			return message.reply('please do provide a number between 2 and 100 for the number of messages to delete.');
		}
		message.channel.fetchMessages({ 'limit': deleteCount })
			.then(function(list) {
				message.channel.bulkDelete(list);
			})
			.catch(error => message.reply(`Couldn't delete message because of: ${error}`));
	},
};
