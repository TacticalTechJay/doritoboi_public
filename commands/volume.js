module.exports = {
  name: 'volume',
  description: 'It is a volume command for music.',
  guildOnly: true,
  testing: false,
  async execute(message, args, queue) {
    const serverQueue = await queue.get(message.guild.id);
    const voiceChannel = message.member.voiceChannel;
    if (!voiceChannel) return message.channel.send('You need to be in a voice channel to use this command!');
    if (serverQueue && voiceChannel !== message.guild.me.voiceChannel) return message.channel.send('You must be in the same channel as me to play music!');
    if (!message.member.voiceChannel) return message.channel.send('You are not in a voice channel!');
    if (serverQueue.songs[0].requester !== message.author.username) return message.channel.send('Sorry, you can not skip another person\'s song. :frowning:');
    if (!serverQueue) return message.channel.send('There is nothing playing.');
    if (!args[0]) return message.channel.send(`The current volume is: **${serverQueue.volume}%**`);
    if (isNaN(args[0])) return message.channel.send('Please use a number from 1-100 that is not text.');
    if (args[0] < 1 || args[0] > 200) return message.channel.send('Volume must be between 1 - 100');
    serverQueue.volume = args[0];
    serverQueue.connection.dispatcher.setVolumeLogarithmic(args[0] / 100);
    return message.channel.send(`I set the volume to: **${args[0]}%**`);
  }
};
