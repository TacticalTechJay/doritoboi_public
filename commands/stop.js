module.exports = {
    name: 'stop',
    description: 'Why skip the song when you can stop it in total?',
    guildOnly: true,
    testing: false,
    args: false,
    cooldown: 15,
    execute(message, a, queue) {
        const serverQueue = queue.get(message.guild.id);
        const voiceChannel = message.member.voiceChannel;
        if (!voiceChannel) return message.channel.send('You need to be in a voice channel to use this command!');
        if (serverQueue && voiceChannel !== message.guild.me.voiceChannel) return message.channel.send('You must be in the same channel as me to play music!');
        if (serverQueue.songs[0].requester !== message.author.username) return message.channel.send('Sorry, you can not stop another person\'s song. :frowning:');
        if (!message.member.voiceChannel) return message.channel.send('You are not in a voice channel!');
        if (!serverQueue) return message.channel.send('There is nothing playing that I could stop for you.');
        serverQueue.songs = [];
        serverQueue.connection.dispatcher.end('Stop command has been used!');
        return message.channel.send('Stopped music! :D');
        // if (!message.member.voiceChannel) return message.channel.send('Please connect to a voice channel.');
        // if (!message.guild.me.voiceChannel) return message.channel.send('Sory, I am not in a voice channel.');
        // if (message.guild.me.voiceChannelID !== message.member.voiceChannelID) return message.channel.send('Sorry, you are not connected to the same channel as me. ;c');
        // message.guild.voiceConnection.disconnect();
        // message.channel.send('I have left the voice channel. <3');
    }
};
