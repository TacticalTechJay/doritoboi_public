module.exports = {
	name: 'kick',
	description: 'Kick naughty doritos. >:C',
	guildOnly: true,
	args: true,
	execute(message, args) {
		const member = message.mentions.members.first() || message.guild.members.get(args[0]);
		if (member.user == message.author) {
			return message.channel.send('Why would you want to kick yoursef? :frowning:');
		}
		if (!message.guild.me.hasPermission('KICK_MEMBERS') || !member.bannable) {
			return message.channel.send('I do not have permissions to kick the person! They are either a role above me or I do not have permission to kick.');
		}
		if (!message.member.hasPermission('KICK_MEMBERS', false, true, true)) {
			return message.reply('sorry but you ain\'t got the strength to tag anyone down with this boot.');
		}
		if(!member) {
			return message.reply('you gotta tag dat naughty dorito down before you can use this! (Mention a user!)');
		}
		if(!member) {
			return message.reply('I can\'t pin down the good doritos! O_o ');
		}

		const reason = args.slice(1).join(' ');
		if(!reason) {
			return member.kick('No reason given.')
				.then(message.reply(`${member.user.tag} has been kicked by ${message.author.tag} for the following reason: No reason given.`));
		}
		message.mentions.members.first().kick(reason)
			.catch(error => message.reply(`Sorry, I couldn't kick dorito brudduh for a quite specific reason... Report this to the dorito maker: ${error}`));
		message.reply(`${member.user.tag} has been kicked by ${message.author.tag} for the following reason: ${reason}`);
	},
};