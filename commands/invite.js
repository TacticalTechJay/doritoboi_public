const Discord = require('discord.js');
module.exports = {
    name: 'invite',
    description: 'Invite me into your server!',
    guildOnly: false,
    args: false,
    testing: false,
    execute(message) {
        const embed = new Discord.RichEmbed()
            .setColor(0x00fa69)
            .setTitle('Invite me!')
            .setDescription('Thank you for taking consideration into inviting me! \n [Link is here!](https://discordapp.com/oauth2/authorize?client_id=487090581101740042&scope=bot&permissions=3353606)');
        message.channel.send(embed);
    }
};