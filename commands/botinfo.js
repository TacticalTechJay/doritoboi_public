const Discord = require('discord.js');
module.exports = {
    name: 'botinfo',
    description: 'Info of the bot!',
    guildOnly: false,
    args: false,
    testing: false,
    async execute(message, nothing, important, so, client) {
        // eslint-disable-next-line no-var
        var amount = await client.shard.broadcastEval('this.guilds.size');
        // eslint-disable-next-line no-var
        var amount2 = await amount.reduce((prev, val) => prev + val, 0);
        const embed = new Discord.RichEmbed()
            .setColor(0x0099ff)
            .setTitle('Dorito Bot!')
            .setFooter(message.author.tag)
            .setDescription('Hello! This is just some average moderation/music playing bot! If you wish to invite me, just use `db!invite`(or click below) to add me! I hope you enjoy the bot as much as I enjoyed making it! <3')
            .addField('❤ Total Guilds', amount2, true)
            .addField('💎 Shard ID', ++client.shard.id, true)
            .addField('👑 Creator', 'TacticalTechJay#0001', true)
            .addField('<:refresh:518560519183925268> Version', 'v1.2.0', true)
            .addField('📚 Libary', `Discord.js ${Discord.version}`, true)
            .addField('🖥 OS', require('os').type(), true)
            .addField('👍 Vote', '[DBL Upvote](https://discordbots.org/bot/487090581101740042/vote)', true)
            .addField('<:UpdateReady:518555265281425408> Bot Invite', '[Click here](https://discordapp.com/oauth2/authorize?client_id=487090581101740042&scope=bot&permissions=3353606)', true)
            .addField('<:github:518551810785411082> Source Code', '[Click here](https://github.com/TacticalTechJay/bookish-waffle)', true)
            .addField('📞 Dorito Server', '[Click here](https://discord.gg/zqU3s6Y)', true)
            .addField('☁ Hosted by', '[DigitalOcean](https://m.do.co/c/2e14723f4eef)', true)
            .addField('<:paypal:518550247635550210> Donations', '[Paypal](https://paypal.me/whynotdude)', true);
        message.channel.send(embed);
    }
};
