const { stream } = require('./../config.json');
const ytdl = require('ytdl-core');
module.exports = {
  name: 'sync',
  description: 'sync some music with another bot yo!',
  args: false,
  testing: false,
  guildOnly: true,
  async execute(message) {
    if(!message.member.voiceChannel) return message.channel.send('You sure you\'re in a vc?');
    if (message.guild.id !== '343572980351107077') {
      return message.channel.send('You got the wrong guild! >:O');
    }
    else {
      const vc = await message.client.guilds.get('343572980351107077').channels.get('398491956184875018');
      if (message.member.voiceChannel !== vc) return message.channel.send('You gotta be in the same voice channel as me!');
      var connection = await vc.join();
      connection.playStream(ytdl('https://www.youtube.com/watch?v=oNXzMBA9VU4'), stream)
        .on('end', reason => {
          if (reason === 'Stream is not generating quickly enough.');
          else console.log(reason);
          vc.leave();
        })
        .on('error', err => {
          console.log(err);
        });
    }
  }
};
