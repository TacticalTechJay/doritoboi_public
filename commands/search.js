/* eslint-disable no-shadow */
const ytdl = require('ytdl-core');
const Discord = require('discord.js');
const YouTube = require('simple-youtube-api');
const { stream, yttoken } = require('./../config.json');
const youtube = new YouTube(yttoken);

// let servers = module.exports.servers;
// function play(connection, message) {

// 	servers.dispatcher = connection.playStream(yt(`${servers.queue.url}`, { filter: 'audioonly' }), stream);
// 	servers.queue.shift();

// 	servers.dispatcher.on('end', function() {
// 		if (servers.queue[0]) play(connection, message);
// 		else connection.disconnect();
// 	});

// }
module.exports = {
	name: 'search',
	description: 'Search for music to play! Great ikr?',
	guildOnly: true,
	testing: false,
	cooldown: 2,
	args: true,
	async execute(message, args, queue) {
		// eslint-disable-next-line no-var
		var serverQueue = queue.get(message.guild.id);
		const searchString = args.slice(0).join(' ');
		const voiceChannel = message.member.voiceChannel;
		if (!voiceChannel) return message.channel.send('I\'m sorry but you need to be in a voice channel to play music!');
		if (serverQueue && voiceChannel !== message.guild.me.voiceChannel) return message.channel.send('You must be in the same channel as me to play music!');
		const permissions = voiceChannel.permissionsFor(message.client.user);
		if (!permissions.has('CONNECT')) {
			return message.channel.send('I cannot connect to your voice channel, make sure I have the proper permissions!');
		}
		if (!permissions.has('SPEAK')) {
			return message.channel.send('I cannot speak in this voice channel, make sure I have the proper permissions!');
		}
		try {
			var videos = await youtube.searchVideos(searchString, 10); // eslint-disable-line no-var,no-redeclare
			let index = 0;
			const embed = new Discord.RichEmbed()
				.setColor(0x00c2ec)
				.setTitle('Song selection:')
				.setDescription(videos.map(video2 => `**${++index} -** ${video2.title}`).join('\n') + '\n**Please provide a value to select one of the search results ranging from 1-10.**');
			message.channel.send(embed);
			// eslint-disable-next-line max-depth
			try {
				var response = await message.channel.awaitMessages(message2 => message2.content > 0 && message2.content < 11 && message.author == message2.author, { // eslint-disable-line no-var
					maxMatches: 1,
					time: 10000,
					errors: ['time']
				});
			}
			catch (err) {
				console.error(err);
				return message.channel.send('No or invalid value entered, cancelling video selection.');
			}
			const videoIndex = parseInt(response.first().content);
			// eslint-disable-next-line no-redeclare,no-var
			var video = await youtube.getVideoByID(videos[videoIndex - 1].id);
		}
		catch (err) {
			console.error(err);
			return message.channel.send('🆘 I could not obtain any search results.');
		}
	return handleVideo(video, message, voiceChannel);
		// eslint-disable-next-line no-shadow
	async function handleVideo(video, message, voiceChannel, playlist = false) {
		const serverQueue = queue.get(message.guild.id);
		const song = {
			id: video.id,
			title: video.title,
			url: `https://www.youtube.com/watch?v=${video.raw.id}`,
			requester: message.author.username
		};
		if (!serverQueue) {
			const queueConstruct = {
				textChannel: message.channel,
				voiceChannel: voiceChannel,
				connection: null,
				songs: [],
				volume: 5,
				playing: true
			};
			queue.set(message.guild.id, queueConstruct);

			queueConstruct.songs.push(song);

			try {
				const connection = await voiceChannel.join();
				queueConstruct.connection = connection;
				play(message.guild, queueConstruct.songs[0]);
			}
			catch (error) {
				console.error(`I could not join the voice channel: ${error.stack}`);
				queue.delete(message.guild.id);
				return message.channel.send(`I could not join the voice channel: ${error}`);
			}
		}
		else {
			serverQueue.songs.push(song);
			console.log(serverQueue.songs);
			if (playlist) return undefined;
			else return message.channel.send(`✅ **${song.title}** has been added to the queue!`);
		}
		return undefined;
	}
	function play(guild, song) {
		const serverQueue = queue.get(guild.id);
		if (!song) {
			serverQueue.voiceChannel.leave();
			queue.delete(guild.id);
			return;
		}
		const dispatcher = serverQueue.connection.playStream(ytdl(`${song.url}`, { filter: 'audioonly', quality: 'highestaudio' }), stream)
			.on('end', reason => {
				if (reason === 'Stream is not generating quickly enough.');
				else console.log(reason);
				serverQueue.songs.shift();
				play(guild, serverQueue.songs[0]);
			})
			.on('error', error => console.error(error));
		dispatcher.setVolumeLogarithmic(2 / 5);

		const embed = new Discord.RichEmbed()
			.setColor(0x2daa4b)
			.setTitle('Now playing 🎵')
			.setDescription(`[${song.title}](${song.url})`)
			.setFooter(`Requested by ${message.author.username}`);
		serverQueue.textChannel.send(embed);
		}
		// if (!message.member.voiceChannel) {
		// 	return message.reply('you got to be in a voice channel so I can play music for ya!');
		// }
		// if (!servers) {
		// 	servers = {
		// 	queue: []
		// 	};
		// }
		// const validate = yt.validateURL(args[0]);

		// if (!validate) {
		// 	return message.channel.send('Sorry, please use a youtube video link. It can not be from other websites.');
		// }

		// // server.queue.push(args[0]);
		// yt.getInfo(args[0], function(err, info) {
		// 	if (err) {
		// 		console.error(err);
		// 		return message.channel.send('There was a problem adding to the queue!');
		// 	}
		// 	// console.log(info);
		// 	servers.queue.push({
		// 		url: info.video_url,
		// 		title: info.title,
		// 		requester: message.author.username
		// 	});
		// 	const NowPlayingEmbed = new Discord.RichEmbed()
		// 		.setColor('#00FF00')
		// 		.setTitle('Now playing 🎵')
		// 		.setThumbnail(info.thumbnail_url)
		// 		.setDescription(`[${info.title}](${info.video_url})\nRequested by: ${message.author.username}`);
		// 	message.channel.send(NowPlayingEmbed);
		// });

		// message.member.voiceChannel.join().then(function(connection) {
		// 	play(connection, message);
		// });
	}
};