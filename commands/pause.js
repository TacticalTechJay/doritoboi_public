module.exports = {
    name: 'pause',
    description: 'Pause the music and 🙉',
    args: false,
    guildOnly: true,
    testing: false,
    cooldown: 5,
    execute(message, a, queue) {
        const serverQueue = queue.get(message.guild.id);
        if (serverQueue && serverQueue.playing) {
            serverQueue.playing = false;
            serverQueue.connection.dispatcher.pause();
            return message.channel.send('⏸ Paused the playing music!');
        }
        return message.channel.send('There is nothing playing!');
    }
};