const { RichEmbed } = require('discord.js');
module.exports = {
    name: 'vote',
    description: 'Vote for the bot! :D',
    guildOnly: false,
    testing: false,
    args: false,
    cooldown: 5,
    execute(message) {
        const embed = new RichEmbed()
            .setTitle('Voting!')
            .setDescription('You can vote for me at either of these bot lists below.')
            .addField('Discord Bots List', '[Here! 🎉](https://discordbotlist.com/bots/487090581101740042/upvote)')
            .addField('Discord BOts', '[Here! 🎉](https://discordbots.org/bot/487090581101740042/vote)');
        message.channel.send(embed);
    }
};