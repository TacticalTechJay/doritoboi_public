module.exports = {
    name: 'skip',
    description: 'Skip the currently playing song.',
    guildOnly: true,
    testing: false,
    args: false,
    aliases: ['s'],
    cooldown: 2,
    execute(message, a, queue) {
        const serverQueue = queue.get(message.guild.id);
        const voiceChannel = message.member.voiceChannel;
        if (!voiceChannel) return message.channel.send('You need to be in a voice channel to use this command!');
        if (serverQueue && voiceChannel !== message.guild.me.voiceChannel) return message.channel.send('You must be in the same channel as me to play music!');
        if (serverQueue.songs[0].requester !== message.author.username) return message.channel.send('Sorry, you can not skip another person\'s song. :frowning:');
        if (!message.member.voiceChannel) return message.channel.send('You are not in a voice channel!');
        if (!serverQueue) return message.channel.send('There is nothing playing that I could skip!');
        serverQueue.connection.dispatcher.end('Skip command has been used!');
        return undefined;
    }
};
