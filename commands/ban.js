module.exports = {
	name: 'ban',
	description: 'Ban the super expired doritos.',
	guildOnly: true,
	args: true,
	execute(message, args) {
		const member = message.mentions.members.first() || message.guild.members.get(args[0]);
		if (member.user == message.author) {
			return message.channel.send('Why would you want to ban yourself? :frowning:');
		}
		if (!message.guild.me.hasPermission('BAN_MEMBERS') || !member.bannable) {
			return message.channel.send('Umm, mind if you hand me a ban hammer so I can ban?(Unless they are a role above me. :V)');
		}
		if (!message.member.hasPermission('BAN_MEMBERS', false, true, true)) {
			return message.channel.send('Hold up! Since when can you smash the hammer on someone of higher level than you?');
		}
		if(!member) {
			return message.reply('you have to mention someone!');
		}
		const reason = args.slice(1).join(' ');
		if(!reason) {
			return member.ban('No reason provided.')
				.then(message.channel.send(`${member.user.tag} has been banned by ${message.author.tag} for: No reason provided.`));
		}

		member.ban(reason)
			.catch(error => `Woops! There was a problem banning this person. Be sure to report this error to the dorito maker: ${error}`);
		message.reply(`${member.user.tag} has been banned by ${message.author.tag} for: ${reason}`);
	},
};