module.exports = {
    name: 'partnerships',
    description: 'Displays the current partnerships with bots!',
    guildOnly: false,
    args: false,
    cooldown: 2,
    testing: false,
    async execute(message) {
        const Discord = require('discord.js');
        const embed = new Discord.RichEmbed()
            .setTitle('Current Partnerships')
            .setDescription('[Amazon Alexa Bot](https://discordbots.org/bot/513129952527253504)');
        message.channel.send(embed);
    }
};