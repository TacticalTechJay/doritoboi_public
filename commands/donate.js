module.exports = {
	name: 'donate',
	description: 'Donate for this bot to keep it up. ^w^',
	guildOnly: false,
	testing: false,
	execute(message) {
		message.channel.send({ embed: {
			color: 0x3B7BBF,
			title: 'Donations',
			url: 'https://paypal.me/whynotdude',
			description: 'Donate to Jay to keep this bot up. After all, this bot isn\'t being hosted for free. It will just be an option for you to donate. \n He would not force you to donate. \n [Click here to go to my PayPal to donate if you wish to ^-^.](https://paypal.me/whynotdude)',
			thumbnail: {
				url: 'https://upload.wikimedia.org/wikipedia/commons/a/a4/Paypal_2014_logo.png',
			},
		} })
			.catch(error => message.reply(`There was an error! Report this to the dorito maker pl0x: ${error} `));
	},
};
