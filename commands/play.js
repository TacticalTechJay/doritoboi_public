/* eslint-disable no-shadow */
const YouTube = require('simple-youtube-api');
const { stream, yttoken } = require('./../config.json');
const ytdl = require('ytdl-core');
const youtube = new YouTube(yttoken);
const Discord = require('discord.js');
module.exports = {
    name: 'play',
    description: 'Play music from search or URL!',
    guildOnly: true,
    testing: false,
    args: true,
    aliases: ['p'],
    cooldown: 5,
    async execute(message, args, queue) {
		// eslint-disable-next-line no-var
		var serverQueue = queue.get(message.guild.id);
        const searchString = args.slice(0).join(' ');
        const url = args[0] ? args[0].replace(/<(.+)>/g, '$1') : '';
		const voiceChannel = message.member.voiceChannel;
		if (!voiceChannel) return message.channel.send('You need to be in a voice channel to use this command!');
		if (serverQueue && voiceChannel !== message.guild.me.voiceChannel) return message.channel.send('You must be in the same channel as me to play music!');
        const permissions = voiceChannel.permissionsFor(message.client.user);
        if (!permissions.has('CONNECT')) return message.channel.send('I cannot connect to your voice channel, make sure I have the proper permissions!');
        if (!permissions.has('SPEAK')) return message.channel.send('I cannot speak in this voice channel, make sure I have the proper permissions!');
        if (url.match(/^https?:\/\/(www.youtube.com|youtube.com)\/playlist(.*)$/)) {
			const playlist = await youtube.getPlaylist(url);
			var videos = await playlist.getVideos(); // eslint-disable-line no-var
			for (const video of Object.values(videos)) {
				const video2 = await youtube.getVideoByID(video.id); // eslint-disable-line no-await-in-loop
				await handleVideo(video2, message, voiceChannel, true); // eslint-disable-line no-await-in-loop
			}
			return message.channel.send(`✅ Playlist: **${playlist.title}** has been added to the queue!`);
		}
		else {
			try {
				var video = await youtube.getVideo(url); // eslint-disable-line no-var
			}
			catch (error) {
				try {
					var videos = await youtube.searchVideos(searchString, 1); // eslint-disable-line no-var,no-redeclare
					// eslint-disable-next-line no-redeclare,no-var
					var video = await youtube.getVideoByID(videos[0].id);
				}
				catch (err) {
					console.error(err);
					return message.channel.send('🆘 I cannot seem to get the video you are looking for.');
				}
			}
			return handleVideo(video, message, voiceChannel);
		}
		// eslint-disable-next-line no-shadow
		async function handleVideo(video, message, voiceChannel, playlist = false) {
			const serverQueue = queue.get(message.guild.id);
			const song = {
				id: video.id,
				title: video.title,
				url: `https://www.youtube.com/watch?v=${video.raw.id}`,
				requester: message.author.username,
				thumbnail: video.raw.snippet.thumbnails.default.url
			};
			if (!serverQueue) {
				const queueConstruct = {
					textChannel: message.channel,
					voiceChannel: voiceChannel,
					connection: null,
					songs: [],
					volume: 50,
					playing: true
				};
				queue.set(message.guild.id, queueConstruct);

				queueConstruct.songs.push(song);

				try {
					const connection = await voiceChannel.join();
					queueConstruct.connection = connection;
					play(message.guild, queueConstruct.songs[0]);
				}
				catch (error) {
					console.error(`I could not join the voice channel: ${error.stack}`);
					queue.delete(message.guild.id);
					return message.channel.send(`I could not join the voice channel: ${error}`);
				}
			}
			else {
				const embed = new Discord.RichEmbed()
					.setColor(0x2697ff)
					.setTitle('Added To Queue 🎵')
					.setDescription(`[${song.title}](${song.url})`)
					.setFooter(`Requested by: ${message.author.username}`)
					.setThumbnail(song.thumbnail);
				serverQueue.songs.push(song);
				if (playlist) return undefined;
				else return message.channel.send(embed);
			}
			return undefined;
		}
		function play(guild, song) {
			const serverQueue = queue.get(guild.id);
			if (!song) {
				serverQueue.voiceChannel.leave();
				queue.delete(guild.id);
				return;
			}
			const dispatcher = serverQueue.connection.playStream(ytdl(`${song.url}`), stream)
				.on('end', reason => {
					if (reason === 'Stream is not generating quickly enough.');
					else console.log(reason);
					serverQueue.songs.shift();
					play(guild, serverQueue.songs[0]);
				})
				.on('error', error => console.error(error));
			dispatcher.setVolumeLogarithmic(serverQueue.volume / 100);

			const embed = new Discord.RichEmbed()
				.setColor(0x2daa4b)
				.setTitle('Now playing 🎵')
				.setDescription(`[${song.title}](${song.url})`)
				.setThumbnail(song.thumbnail)
				.setFooter(`Requested by ${message.author.username}`);
			serverQueue.textChannel.send(embed);
		}

    }
};
