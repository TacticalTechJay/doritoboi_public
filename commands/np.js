module.exports = {
    name: 'np',
    description: 'Displays what is currently playing!',
    guildOnly: true,
    testing: false,
    cooldown: 5,
    execute(message, a, queue) {
        const serverQueue = queue.get(message.guild.id);
        if (!serverQueue) return message.channel.send('The entire queue is empty.');
        return message.channel.send(`🎶 Now playing: **${serverQueue.songs[0].title}**`);
    }
};