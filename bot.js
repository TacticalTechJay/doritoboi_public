const fs = require('fs');
const Discord = require('discord.js');
const { prefix, token, dbltoken, dbtoken } = require('./config.json');
const DBL = require('dblapi.js');
const client = new Discord.Client();
const dbl = new DBL(dbltoken, client);
const { exec } = require('child_process');


const queue = new Map();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

const cooldowns = new Discord.Collection();

client.on('ready', () => {
	console.log('Ready!');
	client.user.setActivity('db!help | db!')
		.then(presence => console.log(`Activity is ${presence.game ? presence.game.name : 'none'}`))
		.catch(console.error);
});

const { post } = require('snekfetch');
const updateBotList = async () => {
    console.log('Updating DBL stats');

    const { body: reply } = await post('https://discordbotlist.com/api/bots/487090581101740042/stats')
        .set('Authorization', `Bot ${dbtoken}`)
        .send({
            shard_id: client.shard.id,
            guilds: client.guilds.size,
            users: client.users.size,
            voice_connections: client.voiceConnections.size
        });

    return (reply);
};
	updateBotList()
		.then(r => console.log(r));


dbl.on('posted', () => {
	console.log('Server count posted!');
});

dbl.on('error', e => {
	console.log(`Oops! ${e}`);
});

client.on('message', message => {
	if (!message.content.startsWith(prefix) || message.author.bot) return;
	const args = message.content.slice(prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();
	let serverQueue = '';
	if (message.channel.type == 'test') {
		serverQueue = queue.get(message.guild.id);
	}
	const command = client.commands.get(commandName)
		|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

	if (!command) return;

	if (command.testing && message.author.id != 127888387364487168) {
		return message.reply(`${command.name} is currently being tested and is not fully functional so it is disabled.`);
	}

	if (command.guildOnly && message.channel.type !== 'text') {
		return message.reply('I can\'t execute that command inside DMs!');
	}

	if (command.args && !args.length) {
		let reply = `You didn't provide any arguments, ${message.author}!`;

		if (command.usage) {
			reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
		}

		return message.channel.send(reply);
	}

	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}

	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 3) * 1000;

	if (!timestamps.has(message.author.id)) {
		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}
	else {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
		}

		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}

	try {
		command.execute(message, args, queue, serverQueue, client);
	}
	catch (error) {
		console.error(error);
		message.reply(`there was an error trying to execute that command! Report this to the dorito maker: \`${error}\``);
	}
});

client.on('message', message => {
	const args = message.content.split(' ').slice(1);
	if (message.content.startsWith('db!eval')) {
		if (message.author.id !== '127888387364487168') return;
		const clean = text => {
			if (typeof (text) === 'string') {
			return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
			}
			else{
				return text;
			}
		};
		try {
			const code = args.join(' ');
			let evaled = eval(code);

			if (typeof evaled !== 'string') {
				evaled = require('util').inspect(evaled);
			}
			message.channel.send(clean(evaled), { code:'xl' });
			}
			catch (err) {
				message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
			}
		}
	if (message.content.startsWith('db!exec')) {
		if (message.author.id !== '127888387364487168') return;
		if(!args[0]) return message.channel.send('```COMMAND REQUIRED```');
		try {
			exec(args.join(' '), (err, stdout, stderr) => {
				message.channel.send({ embed: { description: stdout } });
				if (stderr) message.channel.send({ embed: { description: stderr } });
			});
		}
		catch(err) {
			console.log(err);
		}
	}
});

client.login(token);
